import React from 'react'

const index = () => {
    return (
        <div>
            <h1>
                about page
            </h1>
            <form action="/success" name="contact" method="post" data-netlify="true">
                <label for="email">Email:</label>
                <input type="email" name="email"/>
                <label for="nom">Nom:</label>
                <input type="text" name="nom"/>
                <input type="hidden"/>
                <input type="submit" value="envoyer"/>
            </form>
        </div>
    )
}

export default index;