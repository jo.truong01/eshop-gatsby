import React from "react"
import {graphql} from "gatsby"
import { Link } from "gatsby"
import propTypes from "prop-types"

import Layout from "../components/layout"
import SEO from "../components/seo"

export const query = graphql`
  query MyQuery{
    allMarkdownRemark {
      edges {
        node {
          frontmatter {
            title
            price
            description
            image
          }
        }
      }
    }
  }
`

const products = ({data}) => {
    console.log('DATA => ', data)
    return (
<Layout>
    <SEO title="Produits"/>
    <h1>Page de produits</h1>
    <div className="products__grid">
        <table>
            <tbody>
                {
                    data.allMarkdownRemark.edges.map((product) => {
                        return (
                            <div>
                                <div>{product.node.frontmatter.title}</div>
                                <div>{product.node.frontmatter.price}</div>
                                <div>{product.node.frontmatter.description}</div>
                                <div><img src={product.node.frontmatter.image} alt=""/></div>
                            </div>
                        )
                    })
                }
            </tbody>
        </table>
    </div>
    <Link to="/">Retour page accueil</Link>
</Layout>
)}

products.propTypes = {}

export default products;